import './App.css';
import styles from "./style.module.css";
import Listitem from './components/Listitem';
import { useState } from 'react';


function App() {
  const [todo, settodo] = useState("")
  const [todos, settodos] = useState([])
  console.log(todos);
  const handleSubmit = () => {
    settodos([...todos, todo])

  }
  const handleDelete = (todo) => {
    settodos(todos.filter(item => (item !== todo)))
  }

  return (

    <div className={styles.body}>
      <div className={styles.contentWrapper}>
        <div className={styles.titles}>
          <div className={styles.maintitle}><h1>To-do-app!</h1></div>
          <div className={styles.addTitle}><h3>Add new to-do</h3></div>

          <div className={styles.addTodo}>
            {todos.map(todo => (
              <Listitem key={todo} todo={todo} handleDelete={handleDelete} />

            ))}

            <div><input type="text" className={styles.textField} onChange={(e) => settodo(e.target.value)}></input></div>
            <div><button className={styles.add} onClick={handleSubmit}>Add</button></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
