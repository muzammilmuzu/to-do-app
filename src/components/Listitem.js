import {AiFillDelete} from "react-icons/ai";
import './Listitem.css';

export default function Listitem(props){
    const {
        todo , handleDelete} = props;
    return(
        <div className="listitem">
            <h5 className="listview">{todo}</h5>
            <button onClick={()=>handleDelete(todo)} className="delete"><AiFillDelete/></button>
        </div>

    )
    
    }


